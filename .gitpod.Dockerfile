FROM gitpod/workspace-full-vnc

USER gitpod

RUN sudo apt-get update && \
    sudo apt-get install -y libgtk-3-dev libx11-dev libxkbfile-dev libsecret-1-dev libnss3 && \
    sudo rm -rf /var/lib/apt/lists/*

RUN bash -c ". /home/gitpod/.sdkman/bin/sdkman-init.sh && \
    sdk install java 17.0.5-librca && \
    sdk default java 17.0.5-librca"
