package com.pelp88.autoseller.configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(
                title = "Сервис для программы-справочника для организации торгующей автотранспортной техникой",
                version = "0.1",
                description = "API сервиса"
        )
)
public class SwaggerConfig {
}

