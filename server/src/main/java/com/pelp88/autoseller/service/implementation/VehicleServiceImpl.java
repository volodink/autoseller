package com.pelp88.autoseller.service.implementation;

import com.pelp88.autoseller.helper.BeanUtilsHelper;
import com.pelp88.autoseller.mapper.VehicleMapper;
import com.pelp88.autoseller.repository.VehicleRepository;
import com.pelp88.autoseller.rest.dto.VehicleRqDto;
import com.pelp88.autoseller.rest.dto.VehicleRsDto;
import com.pelp88.autoseller.rest.exception.NotFoundException;
import com.pelp88.autoseller.service.VehicleService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jakarta.validation.constraints.NotNull;

@Slf4j
@Service
@AllArgsConstructor
public class VehicleServiceImpl implements VehicleService {

    private static final String VEHICLE_IS_NOT_FOUND_BY_ID_MSG = "Участок с %s = %d не найден";
    private static final String[] IGNORED_ON_COPY_VEHICLES = {"id"};

    private final VehicleRepository vehicleRepository;
    private final VehicleMapper vehicleMapper;

    @Override
    @Transactional
    public VehicleRsDto create(VehicleRqDto vehicleRqDto) {
        var vehicle = vehicleMapper.toEntity(vehicleRqDto);
        var created = vehicleRepository.save(vehicle);
        return vehicleMapper.toDto(created);
    }

    @Override
    @Transactional
    public Page<VehicleRsDto> findAll(Pageable pageable) {
        return vehicleRepository.findAll(pageable)
                .map(vehicleMapper::toDto);
    }

    @Override
    @Transactional
    public VehicleRsDto findById(@NotNull Long id) {
        return vehicleRepository.findById(id)
                .map(vehicleMapper::toDto)
                .orElseThrow(() -> new NotFoundException(String.format(VEHICLE_IS_NOT_FOUND_BY_ID_MSG, "id", id)));

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public VehicleRsDto update(Long id, VehicleRqDto vehicleRqDto) {
        return vehicleRepository.findById(id)
                .map(vehicle -> {
                    var newVehicle = vehicleMapper.toEntity(vehicleRqDto);
                    BeanUtils.copyProperties(newVehicle, vehicle,
                            BeanUtilsHelper.getNullPropertyNames(newVehicle, IGNORED_ON_COPY_VEHICLES));
                    return vehicle;
                })
                .map(vehicleMapper::toDto)
                .orElseThrow(() -> new NotFoundException(String.format(VEHICLE_IS_NOT_FOUND_BY_ID_MSG, "id", id)));
    }

    @Override
    @Transactional
    public void delete(@NotNull Long id) {
        vehicleRepository.findById(id).ifPresent(vehicleRepository::delete);
    }

}
