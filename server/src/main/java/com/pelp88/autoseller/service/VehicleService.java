package com.pelp88.autoseller.service;

import com.pelp88.autoseller.rest.dto.VehicleRqDto;
import com.pelp88.autoseller.rest.dto.VehicleRsDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jakarta.validation.constraints.NotNull;

public interface VehicleService {
    /**
     * Создание ТС
     *
     * @param vehicleRqDto входящее DTO ТС
     * @return исходящее DTO ТС
     */
    VehicleRsDto create(@NotNull VehicleRqDto vehicleRqDto);

    /**
     * Поиск всех ТС
     *
     * @param pageable параметры страницы запроса
     * @return страница найденных ТС
     */
    Page<VehicleRsDto> findAll(Pageable pageable);

    /**
     * Поиск ТС по ID
     *
     * @param id ID ТС
     * @return исходящее DTO ТС
     */
    VehicleRsDto findById(@NotNull Long id);

    /**
     * Обновление ТС по ID
     *
     * @param id ID ТС
     * @param vehicleRqDto входящее DTO ТС
     * @return исходящее DTO ТС
     */
    VehicleRsDto update(@NotNull Long id, @NotNull VehicleRqDto vehicleRqDto);

    /**
     * Удаление ТС
     *
     * @param id ID ТС
     */
    void delete(@NotNull Long id);

}
