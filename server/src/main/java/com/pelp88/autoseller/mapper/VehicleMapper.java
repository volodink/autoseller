package com.pelp88.autoseller.mapper;

import com.pelp88.autoseller.entity.Vehicle;
import com.pelp88.autoseller.rest.dto.VehicleRqDto;
import com.pelp88.autoseller.rest.dto.VehicleRsDto;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

@Component
@AllArgsConstructor
public class VehicleMapper {
    private ModelMapper modelMapper;

    @PostConstruct
    private void init(){
        modelMapper.createTypeMap(VehicleRqDto.class, Vehicle.class);
        modelMapper.createTypeMap(Vehicle.class, VehicleRsDto.class);
    }

    public Vehicle toEntity(VehicleRqDto vehicleRqDto){
        return modelMapper.map(vehicleRqDto, Vehicle.class);
    }

    public VehicleRsDto toDto(Vehicle vehicle){
        return modelMapper.map(vehicle, VehicleRsDto.class);
    }
}
