package com.pelp88.autoseller.rest;

import com.pelp88.autoseller.rest.api.VehicleApi;
import com.pelp88.autoseller.rest.dto.VehicleRqDto;
import com.pelp88.autoseller.rest.dto.VehicleRsDto;
import com.pelp88.autoseller.service.VehicleService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;

@RestController
@Valid
@AllArgsConstructor
public class VehicleController implements VehicleApi {
    private final VehicleService vehicleService;

    @Override
    public ResponseEntity<VehicleRsDto> create(VehicleRqDto vehicleRqDto) {
        var createdVehicle = vehicleService.create(vehicleRqDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdVehicle);
    }

    @Override
    public Page<VehicleRsDto> findAll(Pageable pageable) {
        return vehicleService.findAll(pageable);
    }

    @Override
    public VehicleRsDto update(Long vehicleId, VehicleRqDto vehicleRqDto) {
        return vehicleService.update(vehicleId, vehicleRqDto);
    }

    @Override
    public VehicleRsDto findById(Long vehicleId) {
        return vehicleService.findById(vehicleId);
    }

    @Override
    public ResponseEntity<Void> delete(Long vehicleId) {
        vehicleService.delete(vehicleId);
        return ResponseEntity.accepted().build();
    }

}
