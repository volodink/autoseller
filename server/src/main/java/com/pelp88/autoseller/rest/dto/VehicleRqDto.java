package com.pelp88.autoseller.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VehicleRqDto {
    @NotNull
    @Schema(description = "Марка автомобиля")
    private String label;

    @NotNull
    @Schema(description = "Модель автомобиля")
    private String model;

    @NotNull
    @Schema(description = "Категория регистрации ТС")
    private String category;

    @NotNull
    @Schema(description = "Госномер регистрации ТС")
    private String registrationId;

    @NotNull
    @Schema(description = "Тип ТС")
    private String type;

    @NotNull
    @Schema(description = "Год выпуска ТС")
    private Integer year;

    @NotNull
    @Schema(description = "Наличие прицепа (да/нет)")
    private Boolean sidecar;
}
