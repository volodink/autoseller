package com.pelp88.autoseller.rest.api;

import com.pelp88.autoseller.rest.dto.VehicleRqDto;
import com.pelp88.autoseller.rest.dto.VehicleRsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

@RequestMapping("/vehicle/")
@Tag(name = "Vehicle Controller", description = "Контроллер ТС")
@Validated
public interface VehicleApi {
    @Operation(summary = "Создание ТС", description = "Позволяет создать ТС", method = "POST")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "CREATED",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = VehicleRsDto.class))}),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST"),
    })
    @PostMapping("/create")
    ResponseEntity<VehicleRsDto> create(@RequestBody @Valid VehicleRqDto vehicleRqDto);

    @Operation(summary = "Получение всех ТС", description = "Получение всех ТС", method = "GET")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST"),
            @ApiResponse(responseCode = "404", description = "NOT FOUND")
    })
    @GetMapping
    Page<VehicleRsDto> findAll(@ParameterObject Pageable pageable);

    @Operation(
            summary = "Изменение ТС",
            description = "Позволяет изменить информацию о ТС",
            method = "PUT"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = VehicleRsDto.class))}),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST"),
            @ApiResponse(responseCode = "404", description = "NOT FOUND")
    })
    @PutMapping("/{vehicleId}")
    VehicleRsDto update(@PathVariable Long vehicleId, @RequestBody @NotNull @Valid VehicleRqDto vehicleRqDto);

    @Operation(summary = "Получение ТС",
            description = "Позволяет получить ТС по идентификатору",
            method = "GET")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = VehicleRsDto.class))}),
            @ApiResponse(responseCode = "400", description = "BAD REQUEST"),
            @ApiResponse(responseCode = "404", description = "NOT FOUND"),
    })
    @GetMapping("/{vehicleId}")
    VehicleRsDto findById(@PathVariable Long vehicleId);

    @Operation(summary = "Удаление карточки грядки",
            description = "Позволяет удалить карточку грядки по идентификатору",
            method = "DELETE")
    @ApiResponses({
            @ApiResponse(responseCode = "202", description = "ACCEPTED"),
            @ApiResponse(responseCode = "404", description = "NOT FOUND")
    })
    @DeleteMapping("/{vehicleId}")
    ResponseEntity<Void> delete(@PathVariable Long vehicleId);
}

