package com.pelp88.autoseller.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "vehicle")
@AllArgsConstructor
@NoArgsConstructor
public class Vehicle{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "label")
    private String label;

    @Column(name = "model")
    private String model;

    @Column(name = "category")
    private String category;

    @Column(name = "registration_id")
    private String registrationId;

    @Column(name = "type")
    private String type;

    @Column(name = "production_year")
    private Integer year;

    @Column(name = "sidecar")
    private Boolean sidecar;
}
