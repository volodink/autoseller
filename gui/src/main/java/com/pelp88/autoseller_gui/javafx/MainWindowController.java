package com.pelp88.autoseller_gui.javafx;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pelp88.autoseller_gui.entity.Vehicle;
import com.pelp88.autoseller_gui.entity.VehicleRepository;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.rgielen.fxweaver.core.FxControllerAndView;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@Component
@FxmlView("/MainWindow.fxml")
@Slf4j
@RequiredArgsConstructor
public class MainWindowController {
    @NonNull
    private final FxControllerAndView<EditWindowController, VBox> editWindow;

    @NonNull
    private final VehicleRepository vehicleRepository;

    @NonNull
    private final RestTemplate restTemplate;

    @FXML
    private TableView<Vehicle> mainTable;

    @FXML
    private TableColumn<String, Long> id;

    @FXML
    private TableColumn<String, String> label;

    @FXML
    private TableColumn<String, String> model;

    @FXML
    private TableColumn<String, String> category;

    @FXML
    private TableColumn<String, String> registrationId;

    @FXML
    private TableColumn<String, String> type;

    @FXML
    private TableColumn<String, Integer> year;

    @FXML
    private TableColumn<String, Boolean> sidecar;

    @FXML
    private Button add;

    @FXML
    private Button exit;

    @FXML
    public void initialize() throws JsonProcessingException {
        this.exit.setOnAction(
                actionEvent -> ((Stage) this.exit.getScene().getWindow()).close()
        );

        this.add.setOnAction(
                actionEvent -> this.editWindow.getController().show(new Vehicle(), this)
        );

        this.id.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.label.setCellValueFactory(new PropertyValueFactory<>("label"));
        this.model.setCellValueFactory(new PropertyValueFactory<>("model"));
        this.category.setCellValueFactory(new PropertyValueFactory<>("category"));
        this.registrationId.setCellValueFactory(new PropertyValueFactory<>("registrationId"));
        this.type.setCellValueFactory(new PropertyValueFactory<>("type"));
        this.year.setCellValueFactory(new PropertyValueFactory<>("year"));
        this.sidecar.setCellValueFactory(new PropertyValueFactory<>("sidecar"));

        getVehicleInfo("http://localhost:8080/vehicle/");
    }

    public void getVehicleInfo(String url) throws JsonProcessingException {
        this.mainTable.getItems().clear();

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        JsonNode list = new ObjectMapper().readTree(responseEntity.getBody());
        var vehicles = new ArrayList<Vehicle>();
        var iter = list.path("content").elements();

        while (iter.hasNext()){
            var temp = iter.next().toString();
            vehicles.add(new ObjectMapper().readValue(temp, Vehicle.class));
        }

        log.info("Got vehicle info: " + vehicles);

        vehicleRepository.saveAll(vehicles);

        this.mainTable.getItems().addAll(vehicles);
    }

}
