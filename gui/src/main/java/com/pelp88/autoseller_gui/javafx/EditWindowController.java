package com.pelp88.autoseller_gui.javafx;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pelp88.autoseller_gui.entity.Vehicle;
import com.pelp88.autoseller_gui.entity.VehicleRepository;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
@FxmlView("/EditWindow.fxml")
@Slf4j
@RequiredArgsConstructor
public class EditWindowController {

    @NonNull
    private final RestTemplate restTemplate;

    @NonNull
    private final VehicleRepository vehicleRepository;

    @NonNull
    private MainWindowController mainWindowController;

    @NonNull
    private Vehicle editedVehicle;

    private Stage stage;

    @FXML
    private VBox editWindow;

    @FXML
    private Button save;

    @FXML
    private Button exit;

    @FXML
    private TextField label;

    @FXML
    private TextField model;

    @FXML
    private TextField category;

    @FXML
    private TextField registrationId;

    @FXML
    private TextField type;

    @FXML
    private TextField year;

    @FXML
    private RadioButton sidecarYes;

    @FXML
    private RadioButton sidecarNo;

    @FXML
    public void initialize() {
        this.stage = new Stage();
        this.stage.setScene(new Scene(this.editWindow));
        this.sidecarNo.setSelected(true);

        this.exit.setOnAction(
                actionEvent -> this.stage.close()
        );

        this.sidecarYes.setOnAction(
                actionEvent -> this.sidecarNo.setSelected(false)
        );

        this.sidecarNo.setOnAction(
                actionEvent -> this.sidecarYes.setSelected(false)
        );

        this.save.setOnAction(
                actionEvent -> {
                    try {
                        this.process("http://localhost:8080/vehicle/");
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                }
        );
    }

    public void process(String url) throws JsonProcessingException {
        if (editedVehicle.getId() != null){
            log.info("NOT YET IMPLEMENTED");
        } else {
            processNew(url);
        }
    }

    public void processNew(String url) throws JsonProcessingException {
        var label = this.label.getText();
        var model = this.model.getText();
        var category = this.category.getText();
        var registrationId = this.registrationId.getText();
        var type = this.type.getText();
        Integer year = null;
        try {
            year = Integer.parseInt(this.year.getText());
        } catch (Exception e) {
            showAlertWindow("Введите год в виде числа! (прим. 2000)");
            throw e;
        }
        boolean sidecar = this.sidecarYes.isSelected();

        this.editedVehicle.setLabel(label);
        this.editedVehicle.setModel(model);
        this.editedVehicle.setCategory(category);
        this.editedVehicle.setRegistrationId(registrationId);
        this.editedVehicle.setType(type);
        this.editedVehicle.setYear(year);
        this.editedVehicle.setSidecar(sidecar);

        var alreadyInBase = false;

        for (var existingVehicle : vehicleRepository.findAll()){
            if (existingVehicle.equals(this.editedVehicle)) {
                alreadyInBase = true;
                break;
            }
        }

        if (alreadyInBase){
            showAlertWindow("Данное ТС уже есть в базе!");
        } else {
            String json = new ObjectMapper().writeValueAsString(this.editedVehicle);
            var requestHeaders = new HttpHeaders();
            requestHeaders.add("Content-Type", "application/json");
            var requestEntity = new HttpEntity<>(json, requestHeaders);

            restTemplate.exchange(url + "create",
                    HttpMethod.POST,
                    requestEntity,
                    new ParameterizedTypeReference<>() {
                    });

            showAlertWindow("Новое ТС было добавлено в базу!");
            this.mainWindowController.getVehicleInfo(url);
        }
    }

    public void show(Vehicle vehicle, MainWindowController mainWindowController){
        this.mainWindowController = mainWindowController;
        this.editedVehicle = vehicle;
        this.stage.show();
    }

    public void showAlertWindow(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Предупреждение");
        alert.setHeaderText("Предупреждение");
        alert.setContentText(message);

        alert.showAndWait();
    }
}
