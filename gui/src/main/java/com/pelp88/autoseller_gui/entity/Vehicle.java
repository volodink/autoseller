package com.pelp88.autoseller_gui.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.stereotype.Component;

@JsonIgnoreProperties(ignoreUnknown = true)
@Component
@Data
public class Vehicle {
    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "label")
    private String label;

    @JsonProperty(value = "model")
    private String model;

    @JsonProperty(value = "category")
    private String category;

    @JsonProperty(value = "registrationId")
    private String registrationId;

    @JsonProperty(value = "type")
    private String type;

    @JsonProperty(value = "year")
    private Integer year;

    @JsonProperty(value = "sidecar")
    private Boolean sidecar;
}
