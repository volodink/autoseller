module ru.coderiders.teamtask {
    requires javafx.controls;
    requires javafx.fxml;
    requires spring.boot;
    requires com.fasterxml.jackson.databind;
    requires net.rgielen.fxweaver.spring;
    requires lombok;
    requires org.slf4j;
    requires spring.web;
    requires spring.core;
    requires spring.beans;
    requires net.rgielen.fxweaver.core;
    requires spring.context;
    requires spring.boot.autoconfigure;


    exports com.pelp88.autoseller_gui.entity;
    opens com.pelp88.autoseller_gui.entity to javafx.fxml;
    exports com.pelp88.autoseller_gui.javafx;
    opens com.pelp88.autoseller_gui.javafx to javafx.fxml;
    exports com.pelp88.autoseller_gui.configuration;
    opens com.pelp88.autoseller_gui.configuration to javafx.fxml;
    exports com.pelp88.autoseller_gui;
    opens com.pelp88.autoseller_gui to javafx.fxml;
}